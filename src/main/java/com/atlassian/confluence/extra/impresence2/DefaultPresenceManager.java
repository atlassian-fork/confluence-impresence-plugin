/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2;


import com.atlassian.confluence.extra.impresence2.reporter.PresenceReporter;
import com.atlassian.plugin.PluginAccessor;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Keeps track of the presence reporters registered.
 */
public class DefaultPresenceManager implements PresenceManager
{
    private final PluginAccessor pluginAccessor;

    public DefaultPresenceManager(PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    public PresenceReporter getReporter(String key)
    {
        return getPresenceReportersMap().get(key);
    }

    private Map<String, PresenceReporter> getPresenceReportersMap()
    {
        List<PresenceReporterModuleDescriptor> dictionaryModuleDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(PresenceReporterModuleDescriptor.class);

        Map<String, PresenceReporter> presenceReporters = new HashMap<String, PresenceReporter>();
        for (PresenceReporterModuleDescriptor presenceReporterModuleDescriptor : dictionaryModuleDescriptors)
        {
            PresenceReporter module = presenceReporterModuleDescriptor.getModule();
            if (!presenceReporters.containsKey(module.getKey()))
                presenceReporters.put(module.getKey(), module);
        }

        return presenceReporters;
    }

    public Collection<PresenceReporter> getReporters()
    {
        return getPresenceReportersMap().values();
    }
}
