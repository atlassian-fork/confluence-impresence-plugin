/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.impresence2.PresenceManager;
import com.atlassian.confluence.extra.impresence2.reporter.ServerPresenceReporter;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.xwork.RequireSecurityToken;

/**
 * Provides configuration for IM services which don't have a central server.
 */
@WebSudoRequired
public abstract class ServerPresenceConfigAction extends ConfluenceActionSupport
{
    private String server;

    private PresenceManager presenceManager;

    public void setPresenceManager(PresenceManager presenceManager)
    {
        this.presenceManager = presenceManager;
    }

    @Override
    public boolean isPermitted()
    {
        // must match AbstractPresenceMacro permission check
        return super.isPermitted()
                && permissionManager.hasPermission(getRemoteUser(), Permission.ADMINISTER, PermissionManager.TARGET_APPLICATION);
    }

    @Override
    public String doDefault() throws Exception
    {
        ServerPresenceReporter reporter = getReporter();
        if (null != reporter)
            setServer(reporter.getServer());
        
        return super.doDefault();
    }

    @Override
    @RequireSecurityToken(true)
    public String execute() throws Exception
    {
        ServerPresenceReporter reporter = getReporter();
        if (reporter == null)
        {
            addActionError(getText("error.general.nosuchreporter", new String[] { getServiceName() }));
            return ERROR;
        }

        reporter.setServer(getServer());

        return SUCCESS;
    }

    protected abstract String getServiceKey();

    protected abstract String getServiceName();

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public String getActionName(String fullClassName)
    {
        return getText("com.atlassian.confluence.extra.impresence2.config.ServerPresenceConfigAction.name",
                new String[] { getServiceName() });
    }

    public ServerPresenceReporter getReporter()
    {
        return (ServerPresenceReporter) presenceManager.getReporter(getServiceKey());
    }

    @SuppressWarnings("unused")
    public boolean isSystemAdministrator()
    {
        return permissionManager.hasPermission(getRemoteUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM);
    }
}
