package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.SametimePresenceReporter;

public class SametimePresenceConfigActionTestCase extends ServerPresenceConfigActionTestCase<SametimePresenceConfigAction, SametimePresenceReporter>
{
    protected SametimePresenceConfigAction createAction()
    {
        SametimePresenceConfigAction action = new SametimePresenceConfigAction()
        {
            protected String getServiceName()
            {
                return "Sametime";
            }

            public String getText(String key, Object[] substitutions)
            {
                return key;
            }
        };

        action.setPresenceManager(presenceManager);
        return action;
    }

    protected String getServiceKey()
    {
        return "sametime";
    }

    protected Class<SametimePresenceReporter> getPresenceReporterClass()
    {
        return SametimePresenceReporter.class;
    }
}
