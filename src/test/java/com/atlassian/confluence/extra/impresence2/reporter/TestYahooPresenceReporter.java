package com.atlassian.confluence.extra.impresence2.reporter;

import java.io.IOException;

public class TestYahooPresenceReporter extends AbstractPresenceReporterTest<YahooPresenceReporter>
{

    private static final String ICON_HTML =
        "<a title=\"Yahoo! foo\" href=\"ymsgr:sendIM?foo\">\n" +
            "<img border=0  src=\"http://opi.yahoo.com/online?u=foo&m=g&t=0\" style='margin:0px 3px; vertical-align:bottom;' height='12' width='12' >" +
        "</a>";
    private static final String USERNAME_HTML = "<a href=\"ymsgr:sendIM?foo\">foo</a>";

    protected String getPresenceReporterKey()
    {
        return YahooPresenceReporter.KEY;
    }

    protected YahooPresenceReporter createPresenceReporter()
    {
        return new YahooPresenceReporter(localeSupport);
    }

    public void testGetKey()
    {
        assertEquals("yahoo", presenceReporter.getKey());
    }

    public void testGetName()
    {
        assertEquals("presencereporter.yahoo.name", presenceReporter.getName());
    }

    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.yahoo.servicehomepage", presenceReporter.getServiceHomepage());
    }

    public void testHasConfig()
    {
        assertEquals(false, presenceReporter.hasConfig());
    }

    public void testRequiresConfig()
    {
        assertEquals(false, presenceReporter.requiresConfig());
    }

    public void testGetPresenceXHTMLWhenIdIsNotSpecified() throws IOException, PresenceException
    {
        assertTrue(presenceReporter.getPresenceXHTML(null, false).indexOf("presencereporter.yahoo.error.noyahooid") >= 0);
    }

    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        assertEquals(ICON_HTML + "&nbsp;" + USERNAME_HTML, presenceReporter.getPresenceXHTML("foo", true));
    }

    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        assertEquals(ICON_HTML, presenceReporter.getPresenceXHTML("foo", false));
    }
}