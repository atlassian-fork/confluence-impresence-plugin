package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

public abstract class AbstractServerPresenceReporterTest<T extends PresenceReporter>
        extends AbstractPresenceReporterTest<T>
{
    static final String SERVER_NAME = "extra.im.server.name.";

    public void testRequiresConfigWhenServerNotConfigured()
    {
        when(bandanaManager.getValue(eq(ConfluenceBandanaContext.GLOBAL_CONTEXT), anyString())).thenReturn(null);
        assertTrue(createPresenceReporter().requiresConfig());
    }

    public void testRequiresConfigWhenServerConfigured()
    {
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                SERVER_NAME + getPresenceReporterKey()
        )).thenReturn("imaginary-server");

        assertFalse(createPresenceReporter().requiresConfig());
    }

    protected abstract T createPresenceReporter();
}