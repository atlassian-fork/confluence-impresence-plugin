package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.mockito.Mock;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

public class TestSameTimePresenceReporter extends AbstractServerPresenceReporterTest<SametimePresenceReporter>
{

    private Map<String, Object> velocityContext;

    @Mock
    private VelocityHelperService velocityHelperService;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                SERVER_NAME + getPresenceReporterKey()
        )).thenReturn("imaginary-server");

        velocityContext = new HashMap<String, Object>();
    }

    protected String getPresenceReporterKey()
    {
        return SametimePresenceReporter.KEY;
    }

    protected SametimePresenceReporter createPresenceReporter()
    {
        return new SametimePresenceReporter(localeSupport, bandanaManager, velocityHelperService);
    }

    public void testGetKey()
    {
        assertEquals("sametime", createPresenceReporter().getKey());
    }

    public void testGetName()
    {
        assertEquals("presencereporter.sametime.name", createPresenceReporter().getName());
    }

    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.sametime.servicehomepage", createPresenceReporter().getServiceHomepage());
    }

    public void testHasConfig()
    {
        assertEquals(true, createPresenceReporter().hasConfig());
    }

    public void testGetPresenceXHTMLWithoutSpecifyingId() throws IOException, PresenceException
    {
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(velocityContext);

        createPresenceReporter().getPresenceXHTML(null, true);

        assertNotNull(velocityContext);
        assertTrue(velocityContext.containsKey("user"));
        assertNull(velocityContext.get("user"));
    }

    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(velocityContext);

        createPresenceReporter().getPresenceXHTML(null, true);
        assertNotNull(velocityContext);
        assertEquals(Boolean.TRUE, velocityContext.get("outputId"));
    }

    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(velocityContext);

        createPresenceReporter().getPresenceXHTML(null, false);
        assertNotNull(velocityContext);
        assertEquals(Boolean.FALSE, velocityContext.get("outputId"));
    }
}