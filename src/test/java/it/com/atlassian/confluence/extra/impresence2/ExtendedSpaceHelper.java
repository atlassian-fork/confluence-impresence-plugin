package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

/**
 * TODO move into func-test
 */
public class ExtendedSpaceHelper extends SpaceHelper {
    private static final Logger log = LoggerFactory.getLogger(ExtendedSpaceHelper.class);

    public static ExtendedSpaceHelper createSpaceHelper(ConfluenceWebTester confluenceWebTester, String spaceKey)
    {
        return null != spaceKey
                ? new ExtendedSpaceHelper(confluenceWebTester, spaceKey)
                : new ExtendedSpaceHelper(confluenceWebTester);
    }

    public static ExtendedSpaceHelper createSpaceHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createSpaceHelper(confluenceWebTester, null);
    }

    public ExtendedSpaceHelper(ConfluenceWebTester confluenceWebTester) {
        super(confluenceWebTester);
    }

    public ExtendedSpaceHelper(ConfluenceWebTester confluenceWebTester, String spaceKey) {
        super(confluenceWebTester, spaceKey);
    }

    public boolean createWithDefaultPermissions() {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            confluenceSoapService.addSpaceWithDefaultPermissions(soapSessionToken, this.toRemoteSpace());

            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            log.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            log.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            log.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            log.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }
}
