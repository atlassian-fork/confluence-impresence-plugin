package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.helper.AbstractHelper;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

/**
 * TODO move to functest
 */
public class SpacePermissionsHelper extends AbstractHelper {
    private static final Logger log = LoggerFactory.getLogger(SpacePermissionsHelper.class);

    public static SpacePermissionsHelper createSpacePermissionsHelper(ConfluenceWebTester confluenceWebTester) {
        return new SpacePermissionsHelper(confluenceWebTester);
    }

    private String key;

    public SpacePermissionsHelper(ConfluenceWebTester confluenceWebTester) {
        super(confluenceWebTester);
    }

    public boolean addPermission(String groupOrUser, String permission) {
        String soapSessionToken = null;
        try {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            ConfluenceSoapService confluenceSoapService = getConfluenceWebTester().getConfluenceSoapService();
            return confluenceSoapService.addPermissionToSpace(soapSessionToken, permission, groupOrUser, key);
        }
        catch (final MalformedURLException mUrlE)
        {
            log.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            log.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            log.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            log.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }
        return false;
    }

    @Override
    public boolean create() {
        return false;
    }

    @Override
    public boolean read() {
        return false;
    }

    @Override
    public boolean update() {
        return false;
    }

    @Override
    public boolean delete() {
        return false;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
